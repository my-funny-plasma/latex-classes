%% Identification
%% The class identifies itself and the LaTeX version needed
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{icap-aphys_article}[2019/05/05 Class, which accepts requirements of KNU REX ICAP and APHYS conferences (https://indico.knu.ua/category/2/ and https://indico.knu.ua/category/5/)]

%%Preliminary definitions, needed by the options
\newcommand{\headlinecolor}{\normalcolor}
% \LoadClass{article}

\def\@@ptsize{11pt}

%% %%This parts handles the options passed to the class.
%% \DeclareOption{onecolumn}{\OptionNotUsed}
%% \DeclareOption{green}{\renewcommand{\headlinecolor}{\color{green}}}
%% \DeclareOption{red}{\renewcommand{\headlinecolor}{\color{slcolor}}}
\DeclareOption{10pt}{\def\@@ptsize{10pt}}
\DeclareOption{11pt}{\def\@@ptsize{11pt}}
\DeclareOption{12pt}{\def\@@ptsize{12pt}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{extarticle}}
\ProcessOptions\relax

\LoadClass[%a4paper,
  \@@ptsize,
  % onehalfspacing,
  % headsepline,
  % notitlepage,
  % normalheadings,
  % twoside,
  % DIVcalc
  ]{extarticle}

% %%   packages
\RequirePackage{tempora}  % Times for numbers in math mode
\RequirePackage{newtxmath}

\RequirePackage[compact]{titlesec}
\RequirePackage{mdwlist}

\RequirePackage[left=2cm, right=2cm, top=2.5cm, bottom=3cm]{geometry}
\RequirePackage[affil-it]{authblk}

% \renewcommand\Authfont{\bfseries \normalsize}
\renewcommand\Affilfont{\mdseries \itshape \normalsize}

% %% % \usepackage{hyperref}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T2A, T1]{fontenc}
% %% %% % \usepackage{lmodern}
\RequirePackage{graphicx}
\RequirePackage{wrapfig}
\RequirePackage{amssymb}
% %% %% %% \usepackage{hyperref}
\RequirePackage[english, ukrainian]{babel}

\pagenumbering{gobble} % remove page numbering. Variants: r(R)oman, a(A)rabic, a(A)lph
\titleformat*{\section}{\center \bfseries \normalsize}
\titleformat*{\subsection}{\center \bfseries \normalsize}

\titlespacing{\section}{0pt}{*0}{*0}
\titlespacing{\subsection}{0pt}{*0}{*0}
\titlespacing{\subsubsection}{0pt}{*0}{*0}

%% %% % \usepackage{cite}

%% %% % \usepackage{multicol}
%% %% % \setlength{\columnsep}{1cm}

\newcommand*{\email}[1]{
  \normalsize email: {#1}\par
}

\RequirePackage[square,sort&compress,comma,numbers]{natbib}

% make bibliography compact
\setlength{\bibsep}{0pt plus 0.3ex}
\bibliographystyle{ieeetr}

%% \renewcommand{\thesection}{}
%% \renewcommand{\thesubsection}{} % {\arabic{section}.\arabic{subsection}}
%% \renewcommand{\thesubsubsection}{}

%% %% \renewcommand{\familydefault}{\sfdefault}
%% % \setmainfont{Times}

\RequirePackage[format=plain,
labelfont=it,
textfont=it,
figurename=Fig.]{caption}

\makeatletter
\renewcommand{\maketitle}{
\begin{center}
{\normalsize\bfseries\MakeUppercase\@title\par}
\medskip
{\normalsize\bfseries\@author\par}
\end{center}
}
\makeatother

\setlength{\parindent}{0.5cm}
\setlength{\parskip}{0cm}

%% set abstract title
% \renewcommand{\abstractname}{Very good abstract}

\RequirePackage{indentfirst}

\renewenvironment{abstract}
{\list{}{
    \setlength{\leftmargin}{0px}
    \setlength{\rightmargin}{\leftmargin}
    \itshape
  }%
  \item\relax}
{\endlist}


%% reset footnote numeration on each page (begin from 1)
\RequirePackage{perpage}
\MakePerPage{footnote} %the perpage package command

% \newcommand\contentsname{Contents}
% \newcommand\listfigurename{List of Figures}
% \newcommand\listtablename{List of Tables}
% \newcommand\bibname{Bibliography}
% \newcommand\indexname{Index}
% \newcommand\figurename{Figure}
% \newcommand\tablename{Table}
% \newcommand\partname{Part}
% \newcommand\chaptername{Chapter}
% \newcommand\appendixname{Appendix}
% \newcommand\abstractname{Abstract}

\renewcommand{\bibsection}{\section*{References}}

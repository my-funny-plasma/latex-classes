%% Identification
%% The class identifies itself and the LaTeX version needed
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{standard_book}[2019/05/24 Standard class for book creation]

%%Preliminary definitions, needed by the options
\newcommand{\headlinecolor}{\normalcolor}
% \LoadClass{article}

\def\@@ptsize{10pt}

%% %%This parts handles the options passed to the class.
%% \DeclareOption{onecolumn}{\OptionNotUsed}
%% \DeclareOption{green}{\renewcommand{\headlinecolor}{\color{green}}}
%% \DeclareOption{red}{\renewcommand{\headlinecolor}{\color{slcolor}}}

\DeclareOption{10pt}{\def\@@ptsize{10pt}}
\DeclareOption{11pt}{\def\@@ptsize{11pt}}
\DeclareOption{12pt}{\def\@@ptsize{12pt}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions\relax

\LoadClass[a5paper,
  10pt,
  titlepage
]{book}

\RequirePackage{csquotes} % for displayquote environment

% %%   packages
\RequirePackage{tempora}  % Times for numbers in math mode
\RequirePackage{newtxmath}

\RequirePackage[compact]{titlesec}
\RequirePackage{mdwlist}

\RequirePackage[left=2.5cm, right=1.5cm, top=1.5cm, bottom=1.5cm]{geometry}
%% \RequirePackage[affil-it]{authblk}
% \renewcommand\Authfont{\bfseries \normalsize}
% \renewcommand\Affilfont{\mdseries \itshape \normalsize}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T2A, T1]{fontenc}
% %% %% % \usepackage{lmodern}
\RequirePackage{graphicx}
\RequirePackage{wrapfig}
\RequirePackage{float}
\RequirePackage{amssymb}
\RequirePackage{soul}
\RequirePackage{longtable}
\RequirePackage{epigraph}
%% \usepackage{hyperref}
\RequirePackage[english,ukrainian]{babel}

% for strikethrough
\RequirePackage{ulem} 

\RequirePackage{fancyhdr}

\pagenumbering{gobble} % remove page numbering. Variants: r(R)oman, a(A)rabic, a(A)lph
%% \pagestyle{headings}
%% \pagenumbering{arabic}

\titleformat*{\section}{\Large\bfseries\center}
\titleformat*{\subsection}{\large\bfseries\center}
\titleformat*{\subsubsection}{\normalsize\bfseries\center}

%% \titlespacing{\section}{0pt}{*0}{*0}
%% \titlespacing{\subsection}{0pt}{*0}{*0}
%% \titlespacing{\subsubsection}{0pt}{*0}{*0}

%% %% % \usepackage{cite}

%% %% % \usepackage{multicol}
%% %% % \setlength{\columnsep}{1cm}

\newcommand*{\email}[1]{
  \normalsize email: {#1}\par
}

\RequirePackage[square,sort&compress,comma,numbers]{natbib}

% make bibliography compact
\setlength{\bibsep}{0pt plus 0.3ex}

%% \renewcommand{\thesection}{}
%% \renewcommand{\thesubsection}{} % {\arabic{section}.\arabic{subsection}}
%% \renewcommand{\thesubsubsection}{}

%% %% \renewcommand{\familydefault}{\sfdefault}
%% % \setmainfont{Times}

\RequirePackage[format=plain,
labelfont=it,
textfont=it]{caption}

%% \makeatletter
%% \renewcommand{\maketitle}{
%% \begin{center}
%% {\normalsize\bfseries\MakeUppercase\@title\par}
%% \medskip
%% {\normalsize\bfseries\@author\par}
%% \end{center}
%% }
%% \makeatother

\setlength{\parindent}{0.5cm}
\setlength{\parskip}{0cm}

%% set abstract title
% \renewcommand{\abstractname}{Very good abstract}

\RequirePackage{indentfirst}

%% \renewenvironment{abstract}
%% {\list{}{
%%     \setlength{\leftmargin}{0px}
%%     \setlength{\rightmargin}{\leftmargin}
%%     \itshape
%%   }%
%%   \item\relax}
%% {\endlist}



% \newcommand\contentsname{Contents}
% \newcommand\listfigurename{List of Figures}
% \newcommand\listtablename{List of Tables}
% \newcommand\bibname{Bibliography}
% \newcommand\indexname{Index}
% \newcommand\figurename{Figure}
% \newcommand\tablename{Table}
% \newcommand\partname{Part}
% \newcommand\chaptername{Chapter}
% \newcommand\appendixname{Appendix}
% \newcommand\abstractname{Abstract}

%% \usepackage{dejavu}
%%\renewcommand{\familydefault}{\sfdefault}
%%\usepackage{blindtext}

\let\originalepigraph\epigraph
\renewcommand\epigraph[2]{\originalepigraph{\tiny\textit{#1}}{\tiny#2}}

\newcommand{\celsius}{\textdegree{}C} %градуси цельсія
\newcommand{\kelvin}{\textdegree{}C} %градуси цельсія


%% \let\originmaketitle\maketitle
%% \renewcommand\maketitle{\originmaketitle\setcounter{page}{1}\pagenumbering{arabic}}

\let\origintableofcontents\tableofcontents
\renewcommand\tableofcontents{\origintableofcontents\setcounter{page}{1}\pagenumbering{arabic}}
\setcounter{tocdepth}{1}
\pagestyle{plain}

\setcounter{secnumdepth}{-1}

%% reset footnote numeration on each page (begin from 1)
\RequirePackage{perpage}
\MakePerPage{footnote} %the perpage package command

\makeatletter
\newcommand*{\rom}[1]{\expandafter\@slowromancap\romannumeral #1@}
\makeatother

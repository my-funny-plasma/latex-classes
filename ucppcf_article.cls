%% Identification
%% The class identifies itself and the LaTeX version needed
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ucppcf_article}[2019/05/05 Class, which accepts requirements of NAS KINR UCPPCF conference (http://www.kinr.kiev.ua/UCPPCF/2019/index4.htm)]

%%Preliminary definitions, needed by the options
\newcommand{\headlinecolor}{\normalcolor}

%% %%This parts handles the options passed to the class.
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{extarticle}}
\ProcessOptions\relax

\LoadClass[a4paper, 12pt]{extarticle}

% %%   packages
\RequirePackage{tempora}  % Times for numbers in math mode
\RequirePackage{newtxmath}

\RequirePackage[compact]{titlesec}
\RequirePackage{mdwlist}

\RequirePackage[left=2.5cm, right=2.5cm, top=2.5cm, bottom=2.5cm]{geometry}
\RequirePackage[affil-it]{authblk}

% \renewcommand\Authfont{\bfseries \normalsize}
\renewcommand\Affilfont{\mdseries \itshape \normalsize}

% %% % \usepackage{hyperref}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T2A, T1]{fontenc}
% %% %% % \usepackage{lmodern}
\RequirePackage{graphicx}
\RequirePackage{wrapfig}
\RequirePackage{amssymb}
% %% %% %% \usepackage{hyperref}
\RequirePackage[english, ukrainian]{babel}

\pagenumbering{gobble} % remove page numbering. Variants: r(R)oman, a(A)rabic, a(A)lph
\titleformat*{\section}{\center \bfseries \normalsize}
\titleformat*{\subsection}{\center \bfseries \normalsize}

\titlespacing{\section}{0pt}{*0}{*0}
\titlespacing{\subsection}{0pt}{*0}{*0}
\titlespacing{\subsubsection}{0pt}{*0}{*0}

%% %% % \usepackage{cite}

%% %% % \usepackage{multicol}
%% %% % \setlength{\columnsep}{1cm}

\newcommand*{\email}[1]{
  \normalsize email: {#1}\par
}

\RequirePackage[square,sort&compress,comma,numbers]{natbib}

% make bibliography compact
\setlength{\bibsep}{0pt plus 0.3ex}
\bibliographystyle{ieeetr}

%% \renewcommand{\thesection}{}
%% \renewcommand{\thesubsection}{} % {\arabic{section}.\arabic{subsection}}
%% \renewcommand{\thesubsubsection}{}

%% %% \renewcommand{\familydefault}{\sfdefault}
%% % \setmainfont{Times}

\RequirePackage[format=plain,
labelfont=it,
textfont=it]{caption}

\makeatletter
\renewcommand{\maketitle}{
\begin{center}
{\normalsize\bfseries\MakeUppercase\@title\par}
\medskip
{\normalsize\@author\par}
\end{center}
}
\makeatother

\setlength{\parindent}{0.5cm}
\setlength{\parskip}{0cm}

%% set abstract title
% \renewcommand{\abstractname}{Very good abstract}

\RequirePackage{indentfirst}

\renewenvironment{abstract}
{\list{}{
    \setlength{\leftmargin}{0px}
    \setlength{\rightmargin}{\leftmargin}
    \itshape
  }%
  \item\relax}
{\endlist}

%% reset footnote numeration on each page (begin from 1)
\RequirePackage{perpage}
\MakePerPage{footnote} %the perpage package command
